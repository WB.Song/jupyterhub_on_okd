# Jupyterhub On OKD
- 해당 리포지토리는 okd에 jupyterhub를 설치한다. 그리고 ldap를 이용하여 jupyterhub의 사용자 인증을 관리하고 jupyter enterprise gateway를 사용하여 각 노트북의 kernel을 외부에서 제공 받는다.

## 목차
1. [tool 설치 - helm, openshift cli](#1-tool-설치)
2. [okd4 setting - 로그인, project 생성, provisioner 추가](#2-okd4-setting)
3. [Enterprise Gateway 설치](#3-enterprise-gateway)
4. [LDAP 설치](#4-ldap-설치)
4. [JupyterHub 설치](#5-jupyterhub-설치)

## 1. tool 설치
### 1) helm 3 설치
- helm verion : version.BuildInfo{Version:"v3.0.2", GitCommit:"19e47ee3283ae98139d98460de796c1be1e3975f", GitTreeState:"clean", GoVersion:"go1.13.5"}
- oc version : oc v3.11.0+0cbc58b

#### Linux
- 아래 명령어를 실행
  - `curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh`
  - `chmod 700 get_helm.sh`
  - `./get_helm.sh`
- 설치 확인 방법
  - `helm version`
![helm version check](https://user-images.githubusercontent.com/15958325/70848110-26aa6500-1eb0-11ea-8da2-2ccaa9dc4ba2.png)
  
#### Windows
- 설치 방법은 바이너리 파일 다운로드와 chocolatey를 이용한 방법이 있음
- 바이너리 파일 다운로드
  - [helm release](https://github.com/helm/helm/releases/tag/v3.0.2)에서 widnow용 바이너리 파일을 다운로드
  - 바이너리 파일의 압축을 해제하고 존재하는 경로를 환경변수에 추가
  - `helm version`을 통해 확인
  
### 2) oc cli 설치
- openshift-origin-client-tool을 [github](https://github.com/openshift/origin/releases)에서 현재 사용중인 OS에 맞는 압축 파일을 다운로드
- 파일의 압축을 풀고 파일이 존재하는 경로를 환경변수에 추가한다.
- `oc version`을 통해 동작을 확인

## 2. OKD4 setting

### kubernetes 환경과 okd와의 차이점
- 기본적으로 사용하는 yaml의 차이점은 없음
- okd에서만 사용하는 object가 존재(ex. DeploymentConfig)
- okd에서는 기본적으로 docker image에서 root 권한으로 실행을 할 수 없어 kubernetes 환경에서 사용하던 이미지를 그대로 사용할 때 권한에 대한 문제가 발생. 따라서 root 권한을 사용하지 않는 이미지를 사용하여 서비스를 구현하거나  실행할 pod에 scc 권한을 부여함 

### 1) OKD 클러스터에 로그인
- oc tool을 사용하기 위해서는 먼저 login을 통해 클러스터와 연결을 해야한다.
  - 아래 명령어를 통해 login
  - oc login <okd 클러스터 domain name or ip adress>
- 로그인 이후부터 helm을 통해 서비스들을 설치할 수 있다.

### 2) Project 생성
- okd에서는 project를 통해 서비스를 논리적으로 분리한다.
- project는 namespace와 동일한 역할을 하고 project의 이름과 namespace의 이름은 동일하다
- project 생성 명령어
  - oc new-project \<project-name\>
- 웹 콘솔에서 project 생성
  - okd 좌측 상단에 Administrator 콘솔에 접속(로그인시 바로 접속)
  - Home -> Project에서 Create Project 버튼 클릭
![new project](./img/okd_webconsole_new_project.PNG)
  - Name에 project 이름을 설정하고 Create 버튼 클릭
  - Display Name과 Description은 별도로 
![new Project](./img/okd_webconsole_new_project_2.PNG)
  - 다른 사용자에게 자신이 생성한 project에 대한 권한 부여
    - Developer 콘솔에 접속
    - More > Project Access 탭 클릭
    - 화면 중간의 Add Access 클릭 후 사용자 이름을 적고과 역할을 선택
    - 부여 가능 역할은 Admin, Edit, View가 있은
![Project Access](./img/okd_project_access_allow.PNG)

### 3) PV 자동 생성을 위한 provisioner 추가
- 종류
  - hostpath : local server에 path를 기반으로 pv를 생성
  - nfs: nfs 서버를 이용하여 pv를 생성
  - 2개 이상의 worker node를 이용하여 클러스터를 구성하면 생성된 pod가 재시작 되었을 때 어떤 node에 생성될 지 보장할 수 없기 때문에 hostpath의 사용을 지양해야한다. pod가 생성된 pv와 다른 node에서 deployment가 된다면 pod 생성에 실패할 수 있다. pod가 어디에 배치될지는 설정을 통해 가능하지만 okd4를 통해 서비스를 배포하는 의미가 없어질 수 있다. nfs를 이용하여 pv를 생성한다면 이러한 문제를 방지할 수 있다.


#### 3-1) 동적 hostpath Persistent Volume 생성을 위한 hostpath-provisioner 설치

- [hostpath-provisioner helm chart](https://github.com/rimusz/charts)
- [hostpath-provisioner docker image source](https://github.com/rimusz/hostpath-provisioner)
- 설치 방법
``` sh
helm repo add rimusz https://charts.rimusz.net
helm repo update
helm upgrade --install hostpath-provisioner -n <namespace> rimusz/hostpath-provisioner
```

- 위 명령어 실행 후 실행된 pod와 sa, sc를 확인 할 수 있다. 
- sa 이름은 hostpath이고 default로 설정된다.
- 이후 생성한 pod에 있는 node에 pvc에서 접근할 path에 권한을 부여해야 한다
  - chmod 777 \<directory path\>
  - chcon -Rt svirt_sandbox_file_t \<your host directory\>
- 문제점
  - 노트북 pod가 hostpath-provisioner와 다른 pod에 생성될 경우에는 생성된 노트북 pod에서 pv에 접근할 수 있는 권한이 없어서 pod가 생성되지 않는다. 같은 node에 존재해야 정상적으로 작동 가능하다.
  
#### 3-2) 동적 NFS Persistent Volumes 생성을 위한 nfs-provisioner 설치

- 사용한 helm chart : [stable/nfs-client-provisioner](https://github.com/helm/charts/tree/master/stable/nfs-client-provisioner)
- nfs provisioner를 사용하기 위해서는 먼저 nfs가 설치되어 있어야 한다.
- 설치 방법
  - scc 권한 부여
    - 권한은 nfs volume에 접근할 수 있는 권한을 부여해야함
    - 권한은 helm을 통해 생성되는 service account인 nfs-client-provisioner와 설치된 namespace에 부여한다.
    - `oc adm policy add-scc-to-user hostmount-anyuid -z nfs-client-provisioner -n <namespace>`
  - project 생성 : `oc new-project nfs-provisioner`
  - `helm upgrade --install nfs-client-provisioner stable/nfs-client-provisioner -f nfs-values.yaml -n nfs-provisioner`


## 3. Enterprise Gateway
### 설치 방법
1. helm chart가 있는 github에서 git clone
  - 원본 차트 위치 https://github.com/jupyter/enterprise_gateway.git
  - 수정된 차트 : https://gitlab.com/aibigdata/helmcharts/-/tree/master/enterprise-gateway_chart
  - `git clone git@gitlab.com:aibigdata/helmcharts.git`

2. values.yaml 작성
- storage 설정
  - enterprise gateway에서 kernel을 customize하기 위하여 pv/pvc 설정을 해준다.
  - 방법은 nfs provision을 통해 자동을 생성하고 생선된 directory에 kernel 설정 정보를 넣어준다.
  - yalm 설정

``` yaml
persistence:
  enabled: true
  accessMode: ReadWriteOnce
  size: 2Gi
  storageClass: "nfs-provisioner" ## nfs-provisioner를 통해 생성한 storage class의 이름
```
- white_list : enterprise gateway에서 jupyterhub에 제공하는 kernel의 목록. whitelist의 이름은 위에서 생성한 pv 내부의 directory 이름이다.
- juptyerhub의 노트북과 pv를 공유하기 위하여 namespace를 공한다. 이 설정은 enterprise-gateway가 생성된 namespace에 kernel pod가 같이 생성된다.
``` yaml
kernel:
  shareGatewayNamespace: true
  whitelist:
    - python_kubernetes
    - python_tf_kubernetes
```
- okd 환경에서 서비스 배포시 대부분의 권한이 scc restrict로 제한되어 있기 때문에 권한을 확인 해야한다. enterprise gateway에서 배포되는 kernel image puller(kip)는 컨테이너 내부에서 docker pull을 이용하여 외부의 docker server에 iamge를 pulling한다. 이 때 컨테이너에 privileged 권한이 없다면 container를 배포할 수 없다. 따라서 yaml에 `kip.securityContext.privileged: true`을 설정하여 권한을 부여한다.
``` yaml
kip:
  securityContext:
    privileged: true
```


3. helm을 이용해 생성될 serviceaccount에 scc 권한 주기
- privileged 권한 추가
  - `oc adm policy add-scc-to-user privileged -z enterprise-gateway-sa -n <namespace name>`
- okd에서는 pod에 privileged 권한을 주기 위해서는 service account를 scc에 추가해야 한다.
  
4. helm install
- namespace는 jupyterhub 서비스를 배포할 namespace와 동일하게 설정
- `helm upgrade --install enterprise-gateway ./helm_chart/enterprise-gateway -f eg-values.yaml -n <jupyterhub의 namespace>`  

5. 생성된 pv에 custom kernel 커널 설정 옮기기
- nfs-server가 마운트 되어 있는 곳에 kernel 설정 파일을 복사한다.


## 4. LDAP 설치
### 1) helm chart 및 value.yaml 준비
1. project 생성
 - OKD setting의 [project 생성](#2\)-Project-생성) 참고
 - project name: ldap
2. helm chart 다운로드
  - helm chart : https://gitlab.com/aibigdata/helmcharts/-/tree/master/openldap
  - enterpirse gateway에서 사용한 helmchart repo에서 openldap chart를 사용
3. openldap-values.yaml 작성

``` yaml
openldap:
  image: osixia/openldap:1.3.0
  pullPolicy: IfNotPresent
  extraEnv:
    LDAP_ORGANISATION: "Northstar Consulting"
    LDAP_DOMAIN: "northstar.co.kr"
storage:
  enabled: true
  storageClass: "nfs-client"
  accessMode: ReadWriteOnce
  size: 2Gi
serviceAccount:
  create: true
  name: ldap-sa
```
- openldap의 extraEnv를 이용하여 LDAP의 조직과 domain 설정
  - LDAP_DOAMIN의 domain으로 기본적인 ldap의 dc가 설정된다.
    - ex) ldap의 id: cn=admin,dc=northstar,dc=co,dc=kr
- storage는 nfs-provisioner를 이용하여 생성
- seriveAccount.name을 설정하지 않으면 default로 생성

4. scc 권한 부여
- openldap는 root 권한으로 실행되기 때문에 anyuid가 필요
- `oc adm policy add-scc-to-user anyuid -z ldap-sa -n ldap`

5. helm istall
  - `helm install ldap ./helm_chart/openldap -f openldap-vaues.yaml -n ldap`

### 2) route 생성
- php ldap admin을 위한 route 설정
  - Administrator 콘솔 > Networking > Routes에서 Create Routes 버튼 클릭
![](./img/okd_webconsole_create_routes.PNG)
  - service에 jupyterhub-ldap, port에 80->80 선택 후 create 클릭
![](./img/okd_webconsole_create_routes_4.PNG)
  - jupyterhub-ldap-admin의 location에 있는 주소를 통해 접속할 수 있다.
![](./img/okd_webconsole_create_routes_3.PNG)
- openldap 설치 후에 계정을 추가해야 jupyterhub에 접속 가능하기 때문에 user를 추가해야한다.

### 3) LDAP 계정 생성
- [php ldap admin을 이용하여 계정 추가하는 방법](https://gitlab.com/songous/data_share/-/blob/master/Infra/jupyter/Jupyterhub/juptyerhub_installation.md#2-openldap%EC%99%80-phpldapadmin-%EC%84%A4%EC%B9%98-%EB%B0%8F-%EC%84%A4%EC%A0%95)
  

## 5. jupyterhub 설치

- scc 권한 추가 : jupyterhub 설치에 사용할 권한 추가
  - jupyterhub를 helm을 통해 설치할 때 자동적으로 sa가 생성되고 이름은 hub이다.
  - `oc adm policy add-scc-to-user anyuid -z hub -n <project>`
  - `oc adm policy add-scc-to-user privileged -z hub -n <project>`
  
### helm 설치를 위한 jupyterhub_values.yaml 파일 작성
- hub pod의 value 설정

``` yaml
hub:
  db:
    type: sqlite-memory
  extraConfig:
    preSpawnHook: |
      def my_hook(spawner):
        username = spawner.user.name
        if '.' in username:
          username = username.replace(".","-2e")
        spawner.environment['KERNEL_VOLUMES'] = "[{name: nb-storage, persistentVolumeClaim: {claimName: claim-"+username+"}}]"
      c.Spawner.pre_spawn_hook = my_hook
      c.Spawner.privileged = True
      c.Spawner.notebook_dir = '/home/jovyan/notebook'
```
- hub.extraconfig의 preSpawnHook을 이용해서 생성할 노트북 pod의 환경 설정
- my_hook을 통해서 enterprise-gateway에서 생성하는 kernel의 volume을 설정
- c.Spawner.privileged : 노트북 pod를 securityContext.privileged의 값을 true로 생성. 노트북 pod에서 sudo를 사용하기 위해서는 privileged 권한일 필요함
- c.Spawnerl.notebook_dir : 주피터 노트북의 실행 디렉토리 설정.

``` yaml
singleuser:
  defaultUrl: "/lab"
  image:
    name: elyra/nb2kg
    tag: 2.0.0
  storage:
    dynamic:
      storageClass: nfs # 사용할 storage class name
  extraEnv:
    KG_URL: "http://'enterprise gateway의 service cluster ip':8888"
    KG_HTTP_USER: jovyan
    KERNEL_USERNAME: jovyan
    KERNEL_VOLUME_MOUNTS: "[{name: nb-storage, mountPath: /home/jovyan/notebook, subPath: notebook}]"
    GRANT_SUDO:  "yes"
    NOTEBOOK_ARGS: " --allow-root "
  uid: 0
  cmd: start-singleuser.sh
  lifecycleHooks:
    postStart:
      exec:
        command: ["mkdir", "-p", "/home/jovyan/notebook"]
```
- enterprise gateway 관련 설정 : 아래 설정은 jupyter notebook의 nb2kg extension 관련 설정
  - `KG_URL` : enterprise-gateway의 cluster ip 설정
  - `KG_HTTP_USER`, "KERNEL_USERNAME" : 커널의 user name
  - `KERNEL_VOLUME_MOUNTS` : kernel의 pod.yaml에서 사용할 volume_mounts의 yaml 값을 환경변수로 설정
- notebook에서 sudo를 사용하기 위한 설정
  - `GRANT_SUDO: yes` : 노트북에 sudo 권한을 부여
  - `NOTEBOOK_ARGS: " --allow-root " ` : jupyter lab을 실행할 때 사용자에게 root 권한을 사용할 수 있도록 설정
  - `uid: 0` : pod를 실행할 때 uid를 0으로 설정. (pod.yaml의 `runAsUser: 0`)
  - `cmd: start-singleuser.sh` : pod에서 실행할 jupyter 실행 shell script. 
- notebook의 볼륨 설정
  - lifecycleHooks은 kubernetes의 기본 기능으로 lifecycle 이벤트를 인지하여 코드를 실행
  - postStart는 컨테이너가 생성된 직후 명령어를 실행. 하지만 hook이 entrypoint에 앞서 실행되는 보장은 없음
  - 위 명령어로 만들어지는 디렉토리는 nfs volume이 mount 되는 디렉토리

``` yaml
proxy:
  service:
    type: NodePort
  secretToken: ""
```
- `service.type: NodePort` : juptyerhub는 외부에서 접근할 때 proxy-public의 service를 통해 접근. okd(openshift)에서는 외부에서 접근할 때 Route를 사용하기 때문에 NodePort가 아닌 ClusterIP로 생성하여도 문제 없음. 
- `proxy.secretToken` : `openssl rand -hex 32`에서 생성된 값으로 변경

- ldap에서 생성할 사용자에 맞춰 template을 작성
  
``` yaml
auth:
  type: ldap
  ldap:
    server:
      address: 'openldap service의 clusterip'
    dn:
      templates: # ldap 설치 시 설정한 LDAP_DOMAIN을 기준으로 작성
        - 'cn={username},cn=jupyter,dc=northstar,dc=co,dc=kr'
        - 'cn={username},dc=northstar,dc=co,dc=kr'
```
- `adress: "cluster ip"` : 위에서 생성한 openldap service의 cluster ip
- `dn.templates` : openldap의 template 형식으로 {username}은 jupyterhub에서 login할 때 사용되는 id이다. 그리고 openldap에서 template 형식으로 인증을 진행하게 된다.

- [jupyterhub_values.yaml](./jupyterhub_values.yaml)
### helm을 이용한 설치
- repo 추가 및 install
  - `helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/`
  - `helm repo update`
  - `helm upgrade --install hub jupyterhub/jupyterhub -f jupyterhub_values.yaml --version=0.9.0 -n <namespace>`
  
- jupyterhub 접속을 위한 routes 설정
  - Administrator 콘솔 > Networking > Routes에서 Create Routes 버튼 클릭
![](./img/okd_webconsole_create_routes.PNG)
  - 아래 그림에서 Service는 hub가 아닌 proxy-public, port는 80->8000을 선택하고 create 버튼을 클릭한다.
![](./img/okd_webconsole_create_routes_2.PNG)
  - jupyterhub의 location에 있는 주소를 통해 jupyterhub가 동작하는 것을 확인할 수 있다.
![](./img/okd_webconsole_create_routes_3.PNG)

### Troubleshooting
- deployment가 정상적으로 올라갔을 때 pod가 생성되지 않는 경우
  - 해당 경우에는 pod가 생성되지 않아 error log를 확인 할 수 없다.
  - 해당 pod의 replicaset에 대한 event를 확인하면 어떠한 error로 pod가 생성되지 않았는지 확인이 가능하다
- pod 생성 도중 directory를 생성하거나 접근 발생하는 Permission denied
  - 해당 컨테이너가 root 권한으로 directory를 접근하거나 생성하려는 경우 Permission denied가 발생할 수 있다. pod가 가지고 있는 service account를 확인하여 scc의 anyuid에 추가하여 root 권한으로 실행할 수 있도록 한다
  - directory에 접근이 불가한 경우는 해당 경로를 hostpath으로 volume을 연결해 두었을 경우 접근이 불가능 할 수 있다. 기본적으로 pod에는 hostpath에 접근이 불가능하다. 위와 비슷하게 scc의 hostaccess에 service account를 추가하면 접근 가능하다\
  - directory가 nfs volume과 연결되어 있을 때는 scc 중 hostmount-anyuid를 추가
  - scc 권한을 부여했을 때는 pod를 삭제하여 다시 시작하거나 서비스 전체를 삭제하고 다시 설치하여야 권한을 부여받아 실행된다.
- 노트북 pod에서 `sudo`를 이용한 `apt-get install`
  - okd에서 user에서 jupyterhub의 기능을 통해 sudo 권한을 부여해도 실제 사용할 때 error 발생
  - ![jupyterhub sudo error](./img/jupyterhub_sudo_error.png)
  - circtl에서 확인 결과 pod 내부에서 `sudo`를 실행하기 위해서는 `securityContext.privileged: true` 설정이 필요
  - jupyterhub에서 노트북 pod를 실행할 때적용되는 service account에 scc privileged를 추가하여도 실제 pod에도 privileged 설정이 별도로 필요.
  - jupyterhub의 config에서 `c.Spawner.privileged = True`를 통해 노트북 pod의 privileged 설정이 가능. 별도의 chart 수정 없이도 실행 시 정상 작동