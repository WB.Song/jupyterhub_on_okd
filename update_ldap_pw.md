# jupyterhub pw 변경방법

## 1. 로그인
![](./img/ldap_login.png)
1. 왼쪽에 login 메뉴 클릭
2. ID, PW 입력
  - ID : cn={JupyterHub id},cn=juptyer,dn=northstar,dn=co,dn=kr
  - PW : Jupyterhug의 비밀번호

## 2. 비밀번호 변경

![](./img/enter_profile.png)
- 왼쪽의 cn={jupyterhub id}를 클릭

![](./img/profile_1.png)
- 자신의 id가 맞는지 확인 후 아래의 비밀번호 변경

![](./img/profile_2.png)
- 자신이 원하는 비밀번호 입력
- 오른쪽 option에서 sha512crypt를 선택
- 입력후 비밀번호 입력창 밑의 `Check password...`를 클릭하여 자신이 입력한 비밀번호 재확인 가능
- 확인 후 화면 가장 아래쪽의 `Update Object` 버튼 클릭

![](./img/update_pw.png)
- 마지막으로 변경사항을 확인하고 `Update Object` 버튼 클릭

## 3. 변경확인
![](./img/ldap_login_2.png)
- 비밀번호 변경시 자동 로그아웃
- 재접속을 통해 로그인하여 정상적으로 변경되었는지 확인 가능
- 로그인이 안될 때는 관리자에게 비밀번호 초기화 요청